export default interface Product {
  id: number;
  image: string;
  name: string;
  subtitle: string;
  price: number;
  num: number;
  amount: number;
}
